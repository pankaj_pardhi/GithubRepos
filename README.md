# GithubRepos

## Assignment:<br/>
Build a mobile application using GitHub api to search through repositories. Filter results. Select a repository and see the details of project and contributors. On selecting contributor, list the repositories tagged to the contributors and from that user should be able to again select his repositories and so on so forth. The application consists of three screens. The home screen where you search and display list of repositories. Repo Details screen where you list of necessary information about the repository. Contributors screen where his/her details and list of repositories are mentioned. Necessary information is listed below.

#### Home:<br/>
* A search bar to search from git API's<br/>
* A recycler view using card view to display the search results.<br/>
* The results are sorted in the descending order of the "watchers" count.<br/>
* The results count should be limited to 10.<br/>
* Clicking on a item to go to Repo Details Activity<br/>
* Dynamic Filters has to be implemented on the results displayed.<br/>
* Implementation/Use-case of the filter is up to your imagination.<br/>

#### Repo Details:<br/>
* This Activity should have a detailed description of the selected item.<br/>
* Details such as Image, Name, Project Link, Description, Contributors should be displayed.<br/>
* When clicked on project link the url should be opened in a In app Browser(Web View)<br/>
* When clicked on a contributor it should  go to Contributor Details<br/>

#### Contributor Details:<br/>
* This Activity should have a detailed description of the contributor.<br/>
* Details such as Contributor Avatar(image),RepoList.<br/>
* Recycler view with card view should be used to display the repo list.<br/>
* Clicking on a item from the repo list it should display the detailed description of the repo in a new Activity (Repo Details).<br/>


GIT DEVELOPERS URL: https://developer.github.com/v3/search/


#### Architecture & Technology Used<br>
* MVP(Model-View-Archtecture)
* Dagger2 (Dependency Injection Framework)

