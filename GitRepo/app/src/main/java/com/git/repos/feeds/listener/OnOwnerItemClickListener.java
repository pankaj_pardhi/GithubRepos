package com.git.repos.feeds.listener;

import com.git.repos.models.Item;
import com.git.repos.models.Owner;

public interface OnOwnerItemClickListener {

    void onItemClicked(Owner owner);
}
