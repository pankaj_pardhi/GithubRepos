package com.git.repos.feeds.bottomSheetDialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.widget.RadioGroup;

import com.git.repos.R;
import com.git.repos.feeds.listener.OnFilterClickListener;

public class FilterBottomSheetDialog extends BottomSheetDialog implements View.OnClickListener {

    private OnFilterClickListener onFilterClickListener;
    private RadioGroup sortRG;
    private RadioGroup orderRG;

    public FilterBottomSheetDialog(@NonNull Context context) {
        super(context);
        init(context);
    }

    public FilterBottomSheetDialog(@NonNull Context context, int theme) {
        super(context, theme);
        init(context);
    }

    protected FilterBottomSheetDialog(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init(context);
    }

    private void init(Context context) {
        View sheetView = getLayoutInflater().inflate(R.layout.dialog_filter, null);
        setContentView(sheetView);

        sheetView.findViewById(R.id.apply).setOnClickListener(this);
        sheetView.findViewById(R.id.close).setOnClickListener(this);
        sortRG = sheetView.findViewById(R.id.sort_by);
        orderRG = sheetView.findViewById(R.id.order_by);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.close) {
            dismiss();
            return;
        } else if (view.getId() == R.id.apply) {
            if (onFilterClickListener != null) {

                String sortStr = null;
                String orderStr = null;
                if (sortRG.getCheckedRadioButtonId() != -1) {
                    sortStr = sortRG.findViewById(sortRG.getCheckedRadioButtonId()).getTag().toString();
                }
                if (orderRG.getCheckedRadioButtonId() != -1) {
                    orderStr = orderRG.findViewById(orderRG.getCheckedRadioButtonId()).getTag().toString();
                }
                onFilterClickListener.onSortClicked(sortStr, orderStr);
            }
            dismiss();
            return;
        }
    }

    public void setOnFilterClickListener(OnFilterClickListener onFilterClickListener) {
        this.onFilterClickListener = onFilterClickListener;
    }
}
