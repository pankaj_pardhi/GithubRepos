package com.git.repos.feeds.listener;

public interface OnRepoSortClickListener {

    void onNewsSortClicked(String sortOrder);
}
