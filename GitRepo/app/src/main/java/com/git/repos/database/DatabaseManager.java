package com.git.repos.database;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public final class DatabaseManager {

    private static final long DATABASE_VERSION = 1;

    private static final String REALM_CONFIG_NAME = "quick.news.realm";

    private static DatabaseManager sDatabaseManager;

    private DatabaseManager(Context context) {
        initRealm(context);
    }

    public static DatabaseManager getInstance(Context context) {
        if (sDatabaseManager == null) {
            synchronized (DatabaseManager.class) {
                sDatabaseManager = new DatabaseManager(context);
            }
        }
        return sDatabaseManager;
    }

    private void initRealm(Context context) {
        Realm.init(context);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name(REALM_CONFIG_NAME)
                .schemaVersion(DATABASE_VERSION)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }
}