package com.git.repos.injection.modules;

import com.git.repos.feeds.RepoDetailPresenter;
import com.git.repos.feeds.interfaces.IRepoDetailActionListener;

import dagger.Module;
import dagger.Provides;

@Module
public class RepoDetailModule {

//    private IRepoListView newsListView;

    public RepoDetailModule() {
    }

//    public NewsFeedModule(IRepoListView newsListView) {
//        newsListView = newsListView;
//    }

//    @Provides
//    IRepoListView newsListView() {
//        return newsListView;
//    }

    @Provides
    IRepoDetailActionListener provideIRepoDetailActionListener(RepoDetailPresenter repoDetailPresenter) {
        return repoDetailPresenter;
    }

//    @Provides
//    IRepoListActionListener newsListActionListener() {
//        return new RepoListPresenter(newsListView);
//    }
}