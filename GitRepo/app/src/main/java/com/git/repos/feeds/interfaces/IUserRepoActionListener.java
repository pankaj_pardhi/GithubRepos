package com.git.repos.feeds.interfaces;


import com.git.repos.models.Item;

public interface IUserRepoActionListener<T> {

    void onResumeActivity();

    void setView(IUserRepoView view);

    void onFilterApply(String filter);

    void onSort(String sortOrder);

    void onRepoItemClicked(Item article);

    void onGetContributors(String url);

}
