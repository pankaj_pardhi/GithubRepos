package com.git.repos.feeds.listener;

import com.git.repos.models.Articles;
import com.git.repos.models.Item;

public interface OnItemClickListener {

    void onItemClicked(Item item);
}
