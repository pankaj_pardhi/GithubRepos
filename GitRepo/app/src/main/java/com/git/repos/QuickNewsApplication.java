package com.git.repos;

import android.app.Application;

import com.git.repos.database.DatabaseManager;
import com.git.repos.injection.components.ApplicationComponent;
import com.git.repos.injection.components.DaggerApplicationComponent;
import com.git.repos.injection.modules.ApplicationModule;


public class QuickNewsApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        DatabaseManager.getInstance(this);
        initializeInjector();
    }

    private void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }

}

