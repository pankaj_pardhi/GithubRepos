package com.git.repos.injection.components;

import com.git.repos.feeds.RepoDetailActivity;
import com.git.repos.injection.PerActivity;
import com.git.repos.injection.modules.ActivityModule;
import com.git.repos.injection.modules.RepoDetailModule;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class, RepoDetailModule.class})
public interface RepoDetailComponent extends ActivityComponent {
    //  void inject(ACIHealthQuestionsFragment healthQuestionsFragment);
//    void inject(RepoListPresenter newsListPresenter);
    void inject(RepoDetailActivity repoDetailActivity);
}
