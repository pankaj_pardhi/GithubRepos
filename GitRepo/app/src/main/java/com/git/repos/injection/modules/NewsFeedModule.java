package com.git.repos.injection.modules;

import com.git.repos.feeds.RepoListPresenter;
import com.git.repos.feeds.interfaces.IRepoListActionListener;

import dagger.Module;
import dagger.Provides;

@Module
public class NewsFeedModule {

//    private IRepoListView newsListView;

    public NewsFeedModule() {
    }

//    public NewsFeedModule(IRepoListView newsListView) {
//        newsListView = newsListView;
//    }

//    @Provides
//    IRepoListView newsListView() {
//        return newsListView;
//    }

    @Provides
    IRepoListActionListener provideINewsListActionListener(RepoListPresenter newsListPresenter) {
        return newsListPresenter;
    }

//    @Provides
//    IRepoListActionListener newsListActionListener() {
//        return new RepoListPresenter(newsListView);
//    }
}