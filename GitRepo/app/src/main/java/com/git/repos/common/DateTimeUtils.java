package com.git.repos.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeUtils {

    public static final String JSON_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    public static final String STANDARD_DATE_FORMAT = "MMMM dd, yyyy";

    public static final String STANDARD_TIME_PATTERN = "00";

    public static String getFormattedDate(String dateStr) {
        SimpleDateFormat sourceDateFormat = new SimpleDateFormat(JSON_DATE_FORMAT);
        SimpleDateFormat targetDateFormat = new SimpleDateFormat(STANDARD_DATE_FORMAT);
        try {
            Date date = sourceDateFormat.parse(dateStr);
            return targetDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }
}
