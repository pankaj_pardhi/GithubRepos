package com.git.repos.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FeedResult {

    @SerializedName("articles")
    private List<Articles> articles;
    @SerializedName("totalResults")
    private String totalResults;
    @SerializedName("status")
    private String status;

    public List<Articles> getArticles() {
        return articles;
    }

    public void setArticles(List<Articles> articles) {
        this.articles = articles;
    }

    public String getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "FeedResult ["
                + "articles = " + articles
                + ", totalResults = " + totalResults
                + ", status = " + status
                + "]";
    }
}
