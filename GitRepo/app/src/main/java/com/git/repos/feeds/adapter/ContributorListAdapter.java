package com.git.repos.feeds.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.git.repos.R;
import com.git.repos.feeds.listener.OnOwnerItemClickListener;
import com.git.repos.models.Owner;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ContributorListAdapter extends RecyclerView.Adapter<ContributorListAdapter.NewsFeedHolder>
        implements View.OnClickListener {

    private static final String TAG = ContributorListAdapter.class.getSimpleName();
    private List<Owner> mOwnerList;
    private Context mContext;
    private OnOwnerItemClickListener mItemClickListener;

    public ContributorListAdapter(Context context) {
        mContext = context;
    }

    public ContributorListAdapter(Context context, OnOwnerItemClickListener itemClickListener) {
        mContext = context;
        mItemClickListener = itemClickListener;
    }

    /**
     * set List of News
     *
     * @param listItems
     */
    public void setList(List<Owner> listItems) {
        mOwnerList = listItems;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mOwnerList != null ? mOwnerList.size() : 0;
    }


    @NonNull
    @Override
    public NewsFeedHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.contributor_list_feed_item, null, false);

        return new NewsFeedHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsFeedHolder holder, int position) {
        bindNewsView(holder, position);
    }

    /**
     * Bind News View
     *
     * @param position
     * @param holder
     */
    private void bindNewsView(NewsFeedHolder holder, int position) {
        Owner owner = mOwnerList.get(position);

        //Bind Title, Published On, Source and Description
        holder.mUserTitle.setText(owner.getLogin());
        holder.mItem.setTag(holder.getAdapterPosition());
        holder.mItem.setOnClickListener(this);

        //Bind Image to ImageView using Glide
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_github_logo);
        requestOptions.error(R.drawable.ic_github_logo);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
        Glide.with(mContext.getApplicationContext())
                .load(owner.getAvatarUrl())
                .apply(requestOptions)
                .into(holder.mUserImage);

    }

    @Override
    public void onClick(View v) {
        if (mItemClickListener != null) {
            int itemPosition = Integer.parseInt(v.getTag().toString());
            mItemClickListener.onItemClicked(mOwnerList.get(itemPosition));
        }
    }


    public class NewsFeedHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView mUserImage;
        @BindView(R.id.title)
        TextView mUserTitle;
        @BindView(R.id.container)
        View mItem;


        public NewsFeedHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
