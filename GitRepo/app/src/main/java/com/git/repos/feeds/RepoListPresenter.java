package com.git.repos.feeds;

import android.text.TextUtils;
import android.util.Log;

import com.git.repos.R;
import com.git.repos.feeds.interfaces.IRepoListActionListener;
import com.git.repos.feeds.interfaces.IRepoListView;
import com.git.repos.injection.PerActivity;
import com.git.repos.models.Articles;
import com.git.repos.models.Item;
import com.git.repos.models.SearchResponse;
import com.git.repos.network.ApiClient;
import com.git.repos.network.ApiInterface;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.constraint.Constraints.TAG;

@SuppressWarnings("WeakerAccess")
@PerActivity
public class RepoListPresenter implements IRepoListActionListener<IRepoListView> {

    private IRepoListView mNewsListView;
    private final Realm mRealm;
    private String mSearchText;
    private String mSortOrder;

    @Inject
    public RepoListPresenter() {
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public void setView(IRepoListView newListView) {
        mNewsListView = newListView;
        mSearchText = "android";
        mSortOrder = mNewsListView.getReposActivityContext().getString(R.string.watcher);
    }

    @Override
    public void onFilterApply(String filter, String order) {
        mSearchText = filter != null ? filter : mSearchText;
        mSortOrder = order != null ? order : mSortOrder;
        getNews(mSearchText, mSortOrder);
    }

    @Override
    public void onResumeActivity() {
//        List<Articles> articles = mRealm.where(Articles.class).findAll();
//        mNewsListView.setReposToList(mRealm.copyFromRealm(articles));

        getNews(mSearchText, mSortOrder);
    }

    @Override
    public void onSort(String sortOrder) {
        mSortOrder = sortOrder;
        getNews(mSearchText, sortOrder);
    }

    @Override
    public void onRepoItemClicked(Item article) {
        mNewsListView.openRepo(article);
    }

    @Override
    public void onSearch(String query) {
        mSearchText = query;
        getNews(mSearchText, mSortOrder);
    }

    /**
     * Call to get News
     */
    private void getNews(String category, String sortOrder) {

        mNewsListView.showLoading();

        ApiInterface apiInterface = ApiClient.getInstance(mNewsListView.getReposActivityContext())
                .getClient().create(ApiInterface.class);

        category = TextUtils.isEmpty(category) ? "git" : category;
        sortOrder = TextUtils.isEmpty(sortOrder) ? "watcher" : sortOrder;

        Call<SearchResponse> call = apiInterface.getRepository(category, sortOrder);
        call.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                mNewsListView.hideLoading();
                if (response.isSuccessful()) {
                    SearchResponse result = response.body();

                    List<Item> items = result.getItems();
                    Collections.sort(items, new Comparator<Item>() {
                        @Override
                        public int compare(Item o1, Item o2) {
                            if (o1.getWatchersCount() - o2.getWatchersCount() > 0) {
                                return -1;
                            } else if (o1.getWatchersCount() - o2.getWatchersCount() == 0) {
                                return 0;
                            } else {
                                return 1;
                            }
                        }
                    });
                    mNewsListView.setReposToList(items);
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                mNewsListView.hideLoading();
            }
        });
    }

    private void saveNewsToLocal(List<Articles> articles) {
        mRealm.beginTransaction();
        mRealm.insertOrUpdate(articles);
        mRealm.commitTransaction();

    }
}
