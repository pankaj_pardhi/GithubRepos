package com.git.repos.injection.components;

import com.git.repos.feeds.RepoFeedsActivity;
import com.git.repos.injection.PerActivity;
import com.git.repos.injection.modules.ActivityModule;
import com.git.repos.injection.modules.NewsFeedModule;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class, NewsFeedModule.class})
public interface NewsComponent extends ActivityComponent {
//  void inject(ACIHealthQuestionsFragment healthQuestionsFragment);
//    void inject(RepoListPresenter newsListPresenter);
    void inject(RepoFeedsActivity repoFeedsActivity);
}
