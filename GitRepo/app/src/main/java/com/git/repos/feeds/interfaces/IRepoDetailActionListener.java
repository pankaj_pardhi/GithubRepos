package com.git.repos.feeds.interfaces;


import com.git.repos.models.Item;

public interface IRepoDetailActionListener<T> {

    void onResumeActivity();

    void setView(IRepoDetailView view);

    void onFilterApply(String filter);

    void onSort(String sortOrder);

    void onRepoItemClicked(Item article);

    void onGetContributors(String url);

}
