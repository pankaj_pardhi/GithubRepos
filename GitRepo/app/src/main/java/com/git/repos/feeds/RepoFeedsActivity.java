package com.git.repos.feeds;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.git.repos.R;
import com.git.repos.feeds.adapter.RepoFeedAdapter;
import com.git.repos.feeds.bottomSheetDialog.FilterBottomSheetDialog;
import com.git.repos.feeds.bottomSheetDialog.SortBottomSheetDialog;
import com.git.repos.feeds.interfaces.IRepoListActionListener;
import com.git.repos.feeds.interfaces.IRepoListView;
import com.git.repos.feeds.listener.OnItemClickListener;
import com.git.repos.feeds.listener.OnFilterClickListener;
import com.git.repos.injection.HasComponent;
import com.git.repos.injection.components.DaggerNewsComponent;
import com.git.repos.injection.components.NewsComponent;
import com.git.repos.injection.modules.NewsFeedModule;
import com.git.repos.models.Item;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RepoFeedsActivity extends BaseActivity implements HasComponent<NewsComponent>,
        IRepoListView, OnFilterClickListener, OnItemClickListener {

    @BindView(R.id.news_recycler_view)
    RecyclerView mNewsRV;

    private NewsComponent newsComponent;
    private ProgressDialog mProgress;

    private FilterBottomSheetDialog mFilterBottomSheetDialog;

    private RepoFeedAdapter mNewsAdapter;

    @Inject
    IRepoListActionListener mNewsListActionListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_feeds);
        ButterKnife.bind(this);

        initializeInjector();
        init();
    }

    private void init() {

        mProgress = new ProgressDialog(RepoFeedsActivity.this);
        mProgress.setMessage(getString(R.string.please_wait));
        mProgress.setCancelable(false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mNewsRV.setLayoutManager(layoutManager);
        mNewsAdapter = new RepoFeedAdapter(this, this);
        mNewsRV.setAdapter(mNewsAdapter);

        mFilterBottomSheetDialog = new FilterBottomSheetDialog(this);
        mFilterBottomSheetDialog.setOnFilterClickListener(this);

        mNewsListActionListener.setView(this);
    }

    //Module Injection
    private void initializeInjector() {
        this.newsComponent = DaggerNewsComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .newsFeedModule(new NewsFeedModule())
                .build();
        newsComponent.inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNewsListActionListener.onResumeActivity();
    }

    @Override
    public void showLoading() {
        mProgress.show();
    }

    @Override
    public void hideLoading() {
        if (mProgress.isShowing()) {
            mProgress.dismiss();
        }
    }

    @Override
    public Context getReposActivityContext() {
        return this;
    }


    @Override
    public void showErrorMessage(String message) {

    }

    @Override
    public void setReposToList(List<Item> repoList) {
        mNewsAdapter.setList(repoList);
    }

    @Override
    public void openRepo(Item repo) {
        openNewsTab(repo.getUrl());
    }

    @Override
    public void onItemClicked(Item item) {
//        mNewsListActionListener.onRepoItemClicked(articles);
        Intent intent = new Intent(this, RepoDetailActivity.class);
        intent.putExtra("item", item);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_menu, menu);

        final MenuItem filter = menu.findItem(R.id.menu_item_filter);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mNewsListActionListener.onSearch(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_item_filter) {
            mFilterBottomSheetDialog.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSortClicked(String category, String order) {
        mNewsListActionListener.onFilterApply(category, order);
    }

    @Override
    public void onOrderClicked(String order) {
        mNewsListActionListener.onSort(order);
    }

    @Override
    public NewsComponent getComponent() {
        return newsComponent;
    }

    private void openNewsTab(String url) {
        Uri uri = Uri.parse(url);

        CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();
        intentBuilder.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        intentBuilder.setSecondaryToolbarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        intentBuilder.setStartAnimations(this, android.R.anim.slide_in_left,
                android.R.anim.slide_out_right);
        intentBuilder.setExitAnimations(this, android.R.anim.slide_in_left,
                android.R.anim.slide_out_right);

        CustomTabsIntent customTabsIntent = intentBuilder.build();
        customTabsIntent.launchUrl(this, uri);
    }
}




