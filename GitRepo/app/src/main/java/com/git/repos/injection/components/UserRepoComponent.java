package com.git.repos.injection.components;

import com.git.repos.feeds.UserReposActivity;
import com.git.repos.injection.PerActivity;
import com.git.repos.injection.modules.ActivityModule;
import com.git.repos.injection.modules.UserRepoModule;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class, UserRepoModule.class})
public interface UserRepoComponent extends ActivityComponent {
    //  void inject(ACIHealthQuestionsFragment healthQuestionsFragment);
//    void inject(RepoListPresenter newsListPresenter);
    void inject(UserReposActivity userReposActivity);
}
