package com.git.repos.feeds.bottomSheetDialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;

import com.git.repos.R;
import com.git.repos.feeds.listener.OnRepoSortClickListener;

public class SortBottomSheetDialog extends BottomSheetDialog implements View.OnClickListener {

    private OnRepoSortClickListener onRepoSortClickListener;

    public SortBottomSheetDialog(@NonNull Context context) {
        super(context);
        init(context);
    }

    public SortBottomSheetDialog(@NonNull Context context, int theme) {
        super(context, theme);
        init(context);
    }

    protected SortBottomSheetDialog(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init(context);
    }

    private void init(Context context) {
        View sheetView = getLayoutInflater().inflate(R.layout.dialog_sort, null);
        setContentView(sheetView);

        sheetView.findViewById(R.id.popular_post).setOnClickListener(this);
        sheetView.findViewById(R.id.new_post).setOnClickListener(this);
        sheetView.findViewById(R.id.old_post).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (onRepoSortClickListener != null) {
            onRepoSortClickListener.onNewsSortClicked(view.getTag().toString());
        }
        dismiss();
    }

    public void setOnRepoSortClickListener(OnRepoSortClickListener onRepoSortClickListener) {
        this.onRepoSortClickListener = onRepoSortClickListener;
    }
}
