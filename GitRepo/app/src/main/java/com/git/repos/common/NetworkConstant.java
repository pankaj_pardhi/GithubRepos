package com.git.repos.common;

public final class NetworkConstant {
    public static final long CONNECTION_TIME_OUT = 45;

    public static final long READ_TIME_OUT = 45;

    public static final long WRITE_TIME_OUT = 45;

    public static final String HEADER_KEY_CONTENT_TYPE = "Content-Type";

    public static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";
}