package com.git.repos.feeds;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.git.repos.R;
import com.git.repos.feeds.adapter.UserRepoAdapter;
import com.git.repos.feeds.interfaces.IUserRepoActionListener;
import com.git.repos.feeds.interfaces.IUserRepoView;
import com.git.repos.feeds.listener.OnItemClickListener;
import com.git.repos.injection.HasComponent;
import com.git.repos.injection.components.DaggerUserRepoComponent;
import com.git.repos.injection.components.UserRepoComponent;
import com.git.repos.injection.modules.UserRepoModule;
import com.git.repos.models.Item;
import com.git.repos.models.Owner;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserReposActivity extends BaseActivity implements HasComponent<UserRepoComponent>,
        IUserRepoView, OnItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.contributor_rv)
    RecyclerView mContributorRV;

    @BindView(R.id.backdrop)
    ImageView mBackdrop;

    private UserRepoComponent repoComponent;
    private ProgressDialog mProgress;

    private UserRepoAdapter mContributorsAdapter;

    @Inject
    IUserRepoActionListener mNewsListActionListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_repo);
        ButterKnife.bind(this);

        initializeInjector();
        init();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void init() {

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mProgress = new ProgressDialog(UserReposActivity.this);
        mProgress.setMessage(getString(R.string.please_wait));
        mProgress.setCancelable(false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mContributorRV.setLayoutManager(layoutManager);
        mContributorsAdapter = new UserRepoAdapter(this, this);
        mContributorRV.setAdapter(mContributorsAdapter);

        mNewsListActionListener.setView(this);

        Owner owner = getIntent().getParcelableExtra("owner");
        getSupportActionBar().setTitle(owner.getLogin());
        mNewsListActionListener.onGetContributors(owner.getRepos_url());

        //Bind Image to ImageView using Glide
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_news);
        requestOptions.error(R.drawable.ic_news);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
        Glide.with(this)
                .load(owner.getAvatarUrl())
                .apply(requestOptions)
                .into(mBackdrop);
    }

    //Module Injection
    private void initializeInjector() {
        this.repoComponent = DaggerUserRepoComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .userRepoModule(new UserRepoModule())
                .build();
        repoComponent.inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        mNewsListActionListener.onResumeActivity();
    }

    @Override
    public void showLoading() {
        mProgress.show();
    }

    @Override
    public void hideLoading() {
        if (mProgress.isShowing()) {
            mProgress.dismiss();
        }
    }

    @Override
    public Context getReposActivityContext() {
        return this;
    }


    @Override
    public void showErrorMessage(String message) {

    }

    @Override
    public void setReposToList(List<Item> repoList) {

        mContributorsAdapter.setList(repoList);
    }

    @Override
    public void openRepo(Item repo) {
        openNewsTab(repo.getUrl());
    }

    @Override
    public void onItemClicked(Item item) {
//        mNewsListActionListener.onRepoItemClicked(articles);
        Intent intent = new Intent(this, RepoDetailActivity.class);
        intent.putExtra("item", item);
        startActivity(intent);

    }

    @Override
    public UserRepoComponent getComponent() {
        return repoComponent;
    }

    private void openNewsTab(String url) {
        Uri uri = Uri.parse(url);

        CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();
        intentBuilder.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        intentBuilder.setSecondaryToolbarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        intentBuilder.setStartAnimations(this, android.R.anim.slide_in_left,
                android.R.anim.slide_out_right);
        intentBuilder.setExitAnimations(this, android.R.anim.slide_in_left,
                android.R.anim.slide_out_right);

        CustomTabsIntent customTabsIntent = intentBuilder.build();
        customTabsIntent.launchUrl(this, uri);
    }
}




