package com.git.repos.feeds.listener;

import com.git.repos.models.Articles;

public interface OnFilterClickListener {

    void onSortClicked(String category, String order);

    void onOrderClicked(String order);
}
