package com.git.repos.feeds.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.git.repos.R;
import com.git.repos.feeds.listener.OnItemClickListener;
import com.git.repos.models.Item;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RepoFeedAdapter extends RecyclerView.Adapter<RepoFeedAdapter.NewsFeedHolder>
        implements View.OnClickListener {

    private static final String TAG = RepoFeedAdapter.class.getSimpleName();
    private List<Item> mNewsList;
    private Context mContext;
    private OnItemClickListener mNewsItemClickListener;

    public RepoFeedAdapter(Context context) {
        mContext = context;
    }

    public RepoFeedAdapter(Context context, OnItemClickListener newsItemClickListener) {
        mContext = context;
        mNewsItemClickListener = newsItemClickListener;
    }

    /**
     * set List of News
     *
     * @param listItems
     */
    public void setList(List<Item> listItems) {
        mNewsList = listItems;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mNewsList != null ? mNewsList.size() : 0;
    }


    @NonNull
    @Override
    public NewsFeedHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.news_list_feed_item, null, false);

        return new NewsFeedHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsFeedHolder holder, int position) {
        bindNewsView(holder, position);
    }

    /**
     * Bind News View
     *
     * @param position
     * @param holder
     */
    private void bindNewsView(NewsFeedHolder holder, int position) {
        Item repo = mNewsList.get(position);

        //Bind Title, Published On, Source and Description
        holder.mNewsTitle.setText(repo.getName());
//        holder.mNewsPublishedOn.setText(DateTimeUtils.getFormattedDate(repo.getCreatedAt()));
        holder.mStarCount.setText(String.valueOf(repo.getStargazersCount()));
        holder.mBranchCount.setText(String.valueOf(repo.getForksCount()));
        holder.mWatchCount.setText(String.valueOf(repo.getWatchersCount()));

        if (!TextUtils.isEmpty(repo.getDescription())) {
            holder.mNewsDescription.setText(repo.getDescription());
            holder.mNewsDescription.setVisibility(View.VISIBLE);
        } else {
            holder.mNewsDescription.setVisibility(View.GONE);
        }

        //Bind Image to ImageView using Glide
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_github_logo);
        requestOptions.error(R.drawable.ic_github_logo);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
        Glide.with(mContext.getApplicationContext())
                .load(repo.getOwner().getAvatarUrl())
                .apply(requestOptions)
                .into(holder.mNewsImage);

        holder.mNewsTitle.setTag(position);
        holder.mNewsTitle.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (mNewsItemClickListener != null) {
            int itemPosition = Integer.parseInt(v.getTag().toString());
            mNewsItemClickListener.onItemClicked(mNewsList.get(itemPosition));
        }
    }


    public class NewsFeedHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.news_image)
        ImageView mNewsImage;
        @BindView(R.id.news_title)
        TextView mNewsTitle;
        @BindView(R.id.news_description)
        TextView mNewsDescription;

        @BindView(R.id.watcher_tv)
        TextView mWatchCount;

        @BindView(R.id.star_tv)
        TextView mStarCount;

        @BindView(R.id.branch_tv)
        TextView mBranchCount;

        public NewsFeedHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
