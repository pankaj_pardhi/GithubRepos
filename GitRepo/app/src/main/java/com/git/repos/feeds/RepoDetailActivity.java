package com.git.repos.feeds;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.git.repos.R;
import com.git.repos.feeds.adapter.ContributorListAdapter;
import com.git.repos.feeds.interfaces.IRepoDetailActionListener;
import com.git.repos.feeds.interfaces.IRepoDetailView;
import com.git.repos.feeds.listener.OnOwnerItemClickListener;
import com.git.repos.injection.HasComponent;
import com.git.repos.injection.components.DaggerRepoDetailComponent;
import com.git.repos.injection.components.RepoDetailComponent;
import com.git.repos.injection.modules.RepoDetailModule;
import com.git.repos.models.Item;
import com.git.repos.models.Owner;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RepoDetailActivity extends BaseActivity implements HasComponent<RepoDetailComponent>,
        IRepoDetailView, OnOwnerItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.contributor_rv)
    RecyclerView mContributorRV;

    @BindView(R.id.link_tv)
    TextView mLinkTV;

    @BindView(R.id.description)
    TextView mDescriptionTV;

    @BindView(R.id.backdrop)
    ImageView mBackdrop;

    private RepoDetailComponent repoComponent;
    private ProgressDialog mProgress;

    private ContributorListAdapter mContributorsAdapter;

    @Inject
    IRepoDetailActionListener mNewsListActionListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_details);
        ButterKnife.bind(this);

        initializeInjector();
        init();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void init() {

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mProgress = new ProgressDialog(RepoDetailActivity.this);
        mProgress.setMessage(getString(R.string.please_wait));
        mProgress.setCancelable(false);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 3);
        mContributorRV.setLayoutManager(layoutManager);
        mContributorsAdapter = new ContributorListAdapter(this, this);
        mContributorRV.setAdapter(mContributorsAdapter);

        mNewsListActionListener.setView(this);

        Item item = getIntent().getParcelableExtra("item");
        mNewsListActionListener.onGetContributors(item.getContributors_url());

        getSupportActionBar().setTitle(item.getName());

        mLinkTV.setText(item.getHtmlUrl());
        mLinkTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNewsTab(mLinkTV.getText().toString());
            }
        });
        mDescriptionTV.setText(item.getDescription());

        //Bind Image to ImageView using Glide
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_news);
        requestOptions.error(R.drawable.ic_news);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
        Glide.with(this)
                .load(item.getOwner().getAvatarUrl())
                .apply(requestOptions)
                .into(mBackdrop);
    }

    //Module Injection
    private void initializeInjector() {
        this.repoComponent = DaggerRepoDetailComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .repoDetailModule(new RepoDetailModule())
                .build();
        repoComponent.inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        mNewsListActionListener.onResumeActivity();
    }

    @Override
    public void showLoading() {
        mProgress.show();
    }

    @Override
    public void hideLoading() {
        if (mProgress.isShowing()) {
            mProgress.dismiss();
        }
    }

    @Override
    public Context getReposActivityContext() {
        return this;
    }


    @Override
    public void showErrorMessage(String message) {

    }

    @Override
    public void setReposToList(List<Owner> repoList) {

        mContributorsAdapter.setList(repoList);
    }

    @Override
    public void openRepo(Item repo) {
        openNewsTab(repo.getUrl());
    }


    @Override
    public RepoDetailComponent getComponent() {
        return repoComponent;
    }

    private void openNewsTab(String url) {
        Uri uri = Uri.parse(url);

        CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();
        intentBuilder.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        intentBuilder.setSecondaryToolbarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        intentBuilder.setStartAnimations(this, android.R.anim.slide_in_left,
                android.R.anim.slide_out_right);
        intentBuilder.setExitAnimations(this, android.R.anim.slide_in_left,
                android.R.anim.slide_out_right);

        CustomTabsIntent customTabsIntent = intentBuilder.build();
        customTabsIntent.launchUrl(this, uri);
    }

    @Override
    public void onItemClicked(Owner owner) {
        Intent intent = new Intent(this, UserReposActivity.class);
        intent.putExtra("owner", owner);
        startActivity(intent);
    }
}




