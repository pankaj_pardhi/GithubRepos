package com.git.repos.feeds.interfaces;

import android.content.Context;

import com.git.repos.models.Item;
import com.git.repos.models.Owner;

import java.util.List;

public interface IRepoDetailView {

    void showLoading();

    void hideLoading();

    Context getReposActivityContext();

    void showErrorMessage(String message);

    void setReposToList(List<Owner> repoList);

    void openRepo(Item repo);
}
