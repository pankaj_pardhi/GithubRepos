package com.git.repos.network;

import com.git.repos.models.Item;
import com.git.repos.models.Owner;
import com.git.repos.models.SearchResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiInterface {

    //-----------------GET GIT REPOS---------------------------

    @GET("search/repositories?order=desc&per_page=10&page=1")
    Call<SearchResponse> getRepository(@Query("q") String searchString,
                                       @Query("sort") String sortParam);

    @GET
    Call<List<Owner>> getContributors(@Url String url);

    @GET
    Call<List<Item>> getRepos(@Url String url);
}
