package com.git.repos.injection.components;

import android.content.Context;

import com.git.repos.injection.modules.ApplicationModule;
import com.git.repos.feeds.BaseActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(BaseActivity baseActivity);

    //Exposed to sub-graphs.
    Context context();
}
