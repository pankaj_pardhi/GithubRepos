package com.git.repos.feeds.interfaces;

import android.content.Context;

import com.git.repos.models.Articles;
import com.git.repos.models.Item;

import java.util.List;

public interface IRepoListView {

    void showLoading();

    void hideLoading();

    Context getReposActivityContext();

    void showErrorMessage(String message);

    void setReposToList(List<Item> repoList);

    void openRepo(Item repo);
}
