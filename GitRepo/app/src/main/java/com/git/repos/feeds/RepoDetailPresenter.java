package com.git.repos.feeds;

import android.util.Log;

import com.git.repos.R;
import com.git.repos.feeds.interfaces.IRepoDetailActionListener;
import com.git.repos.feeds.interfaces.IRepoDetailView;
import com.git.repos.feeds.interfaces.IRepoListView;
import com.git.repos.injection.PerActivity;
import com.git.repos.models.Articles;
import com.git.repos.models.Item;
import com.git.repos.models.Owner;
import com.git.repos.network.ApiClient;
import com.git.repos.network.ApiInterface;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.constraint.Constraints.TAG;

@SuppressWarnings("WeakerAccess")
@PerActivity
public class RepoDetailPresenter implements IRepoDetailActionListener<IRepoDetailView> {

    private IRepoDetailView mNewsListView;
    private final Realm mRealm;
    private String mFilterApplied;
    private String mSortOrder;

    @Inject
    public RepoDetailPresenter() {
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public void setView(IRepoDetailView newListView) {
        mNewsListView = newListView;
        mFilterApplied = mNewsListView.getReposActivityContext().getString(R.string.sports);
        mSortOrder = mNewsListView.getReposActivityContext().getString(R.string.new_to_old_tag);
    }

    @Override
    public void onResumeActivity() {
//        List<Articles> articles = mRealm.where(Articles.class).findAll();
//        mNewsListView.setReposToList(mRealm.copyFromRealm(articles));

    }

    @Override
    public void onFilterApply(String filter) {
        mFilterApplied = filter;
    }

    @Override
    public void onSort(String sortOrder) {
        mSortOrder = sortOrder;
    }

    @Override
    public void onRepoItemClicked(Item article) {
        mNewsListView.openRepo(article);
    }

    @Override
    public void onGetContributors(String url) {
        getContributions(url);
    }

    /**
     * Call to get News
     */
    private void getContributions(String url) {

        mNewsListView.showLoading();

        ApiInterface apiInterface = ApiClient.getInstance(mNewsListView.getReposActivityContext())
                .getClient().create(ApiInterface.class);

        Call<List<Owner>> call = apiInterface.getContributors(url);
        call.enqueue(new Callback<List<Owner>>() {
            @Override
            public void onResponse(Call<List<Owner>> call, Response<List<Owner>> response) {
                mNewsListView.hideLoading();
                if (response.isSuccessful()) {
                    List<Owner> result = response.body();
                    mNewsListView.setReposToList(result);
//
                }
            }

            @Override
            public void onFailure(Call<List<Owner>> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                mNewsListView.hideLoading();
            }
        });
    }

    private void saveNewsToLocal(List<Articles> articles) {
        mRealm.beginTransaction();
        mRealm.insertOrUpdate(articles);
        mRealm.commitTransaction();

    }
}
