package com.git.repos.feeds.interfaces;


import com.git.repos.models.Item;

public interface IRepoListActionListener<T> {

    void onResumeActivity();

    void setView(IRepoListView view);

    void onFilterApply(String filter, String order);

    void onSort(String sortOrder);

    void onRepoItemClicked(Item article);

    void onSearch(String query);
}
