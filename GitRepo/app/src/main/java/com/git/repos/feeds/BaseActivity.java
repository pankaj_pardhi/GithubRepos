package com.git.repos.feeds;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.git.repos.QuickNewsApplication;
import com.git.repos.injection.components.ApplicationComponent;
import com.git.repos.injection.modules.ActivityModule;

/**
 * Base {@link android.app.Activity} class for every Activity in this application.
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getApplicationComponent().inject(this);
    }

    ApplicationComponent getApplicationComponent() {
        return ((QuickNewsApplication) getApplication()).getApplicationComponent();
    }

    ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }
}
