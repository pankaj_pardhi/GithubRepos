package com.git.repos.injection.modules;

import com.git.repos.feeds.RepoDetailPresenter;
import com.git.repos.feeds.UserReposPresenter;
import com.git.repos.feeds.interfaces.IRepoDetailActionListener;
import com.git.repos.feeds.interfaces.IUserRepoActionListener;

import dagger.Module;
import dagger.Provides;

@Module
public class UserRepoModule {

//    private IRepoListView newsListView;

    public UserRepoModule() {
    }

//    public NewsFeedModule(IRepoListView newsListView) {
//        newsListView = newsListView;
//    }

//    @Provides
//    IRepoListView newsListView() {
//        return newsListView;
//    }

    @Provides
    IUserRepoActionListener provideIUserRepoActionListener(UserReposPresenter userReposPresenter) {
        return userReposPresenter;
    }

//    @Provides
//    IRepoListActionListener newsListActionListener() {
//        return new RepoListPresenter(newsListView);
//    }
}